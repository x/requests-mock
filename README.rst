=====================
REPOSITORY DEPRECATED
=====================

Hi, requests-mock development is continuing on github at
https://github.com/jamielennox/requests-mock

Please submit your issues, patches and development there.

The contents of this repository are still available in the Git source code
management system.  To see the contents of this repository before it was
retired, please check out the previous commit with "git checkout HEAD^1".
